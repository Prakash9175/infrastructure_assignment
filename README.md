## Infrastructure_Assignment

### Program will perform 2 Task
1.	Search top 10 largest files
2.	Cleaning Desktop

### For 1st Operation
In windows it will ask for user input for which drive to search among given drives on the system and perform the search. After searching it will list the top 10 largest files along with their size(in MB) in decreasing order.
On Linux it will search the home directory and list the top 10 largest files in decreasing order.

### For 2nd Operation
On both windows and Linux, it will search for files for the given extension(given in code file) on the desktop and create atmost 8 directories(if not present) in Document directory according to given extensions. And finally move these files to their respective directories.  
Directories Created are among the following:  
Image_Files => for image files(extensions: .jpg,.jpeg,.gif etc.)  
MSOffice_files => for MS Office files(extensions: .docx,.xls,.ppt etc.)  
PDF_Files => for Pdf files(extensions: .pdf)  
Video_Files => for Video files(extensions: .mp4,.mkv etc.)  
Text_Files => for Text files(extensions: .txt)  
Music_Files => for Music files(extensions: .mp3,.wav etc.)  
Code_Files => for Coding files(extensions: .htm,.c++,.py etc.)  
Compressed_Files => for Compressed files(extensions: .zip,.7z,.rar etc.)